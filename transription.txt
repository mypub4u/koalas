Hallo Du!
Bevor du loslegst den Talk zu transkribieren, sieh dir bitte noch einmal unseren Style Guide an: https://wiki.c3subtitles.de/de:styleguide. Solltest du Fragen haben, dann kannst du uns gerne direkt fragen oder unter https://webirc.hackint.org/#irc://hackint.org/#subtitles oder https://rocket.events.ccc.de/channel/subtitles oder https://chat.rc3.world/channel/subtitles erreichen.
Bitte vergiss nicht deinen Fortschritt im Fortschrittsbalken auf der Seite des Talks einzutragen.
Vielen Dank für dein Engagement!
Hey you!
Prior to transcribing, please look at your style guide: https://wiki.c3subtitles.de/en:styleguide. If you have some questions you can either ask us personally or write us at https://webirc.hackint.org/#irc://hackint.org/#subtitles or https://rocket.events.ccc.de/channel/subtitles or https://chat.rc3.world/channel/subtitles .
Please don't forget to mark your progress in the progress bar at the talk's website.
Thank you very much for your commitment!
======================================================================
00:00 Intro-Musik

00:16 Herald: Der nächste Speaker ist CodeFreezR mit dem Titel "KOAlitionsvertragsAnalyse - Mit Tools und Methoden dem Ampelvertrag auf den Zahn fühlen". Er ist Enterprise Engineer und inspiriert durch das Känguru - Wie kann man sich besser inspirieren lassen. Er beschreibt sich selbst als Faktenchecker, der Spaß daran hat Zusammenhänger zu erkunden. Wenn ihr Fragen habt könnt ihr die auf IRC, Twitter oder Mastodon stellen. Und dann Film ab, Kamera Action.

Slide 1: (00:00) +51s
Herzlich willkommen ihr Lieben und Liebenden,

in den folgenden 30 Minuten möchte ich Euch die Geschichte von Koala-Kid und mir erzählen, wie wir die quantitative Textanalyse und das explorative Lesen wieder mal entdeckt haben. Diesmal anhand des Koalitionsvertrag Ampel-Regierung den wir im Folgenden einfach KV nennen.

Mein Nick ist CodeFreezR; und los geht's.

Slide 2: (00:25) +51s
Am 24.11. sind drei Dunge passiert:
In der Familie gab es ein Geburtstagsvorfall - Super Nice.
Pietsmiets Kochbuch kam heraus und wir haben es in den Händen gehalten, 
und der Koalitionsvertrag wurde vorgestellt, andere nennen es die "Gelben Seiten".
Am 25.11 ist dieser kleine Comic-Strip auf Twitter retweeted worden und dabei ist mir klar geworden, daß ich mich doch mit dem Koalitionsvertrag auseinandersetzen möchte; Zu merkwürdige Dinge habe ich da im Vorfeld zu gehört. 
Und am 26.11 um 23:42 - Wer kann sich erinnern was ist das für eine Uhrzeit? Genau, das ist der Einsendeschluss für Call-For-Papers gewesen. Da habe ich mir gesagt, komm da reich ich was ein. Um gefühlt 23:40 war meine Einreichung verfertigt und, jesses-jeh, sie wurde auch angenommen.


Slide 3.1: (1:21) +51s
Da haben wir den Salat^^
Und was muss man machen, wenn man einen Vortrag hält und in den Medien ist, man muss was für die Aufmerksamkeitsbewirtschaftung tuen. Ihr kennt das: Schlagzeilen, Skandale, … Irgendwas mussten wir finden. Aber da mussten wir gar nicht lange suchen: Diese 177 Seiten bestehen aus 76 Seiten Luft. Ja so isses. 

Slide 3.2: (1:37) +51s
Deswegen eine Bitte: Druckt das nicht aus! Damit haben wir dann schonmal was in das Kapitel eingezahlt.

Slide 4: (1:49) +51s
Dann standen wir da, mit unserem Engagement, dass wir die Welt zu verändern wollten und hatten den Koalitionsvertrag vorliegen und überlegten was machen wir denn jetzt?
Ok, wir nehmen das mal auseinander als Sourcecode. Das können wir machen, damit kennen wir uns aus. In der Sourcecode-Analyse wird alles Mögliche gezählt, auseinandergenommen und verglichen, das haben wir schon oft gemacht, können wir also hier mit dem Koalitionsvertrag ja auch mal machen.
Aber wie wäre es denn, wenn wir den Koalitionsvertrag als Spezifikation verstehen? Als Spezifikation für die nächste Legislaturperiode.
Oder was ist denn das, wenn wir den Koalitionsvertrag als Objektmodell auseinandernehmen würden? Bringt das was? Koala-Kid habe ich glaub bis heute noch nicht davon überzeugt, aber lassen wir mal schauen.
Und zum Schluss wollen wir uns nochmal anschauen, wie sich der Koalitionsvertrag als Literatur darstellt. Denn ich möchte Euch Auffordern den auch wirklich mal zu lesen. Die Regierung wird uns damit wohl die nächsten vier Jahre martern wollen.

Slide 5.1: (2:56) +51s
KV als Sourcecode. Unser erstes Kapitel in der Reihe von den vier Kapiteln.

Slide 5.2 (3:03)
Der Sourcecode. Wir haben jetzt hier kein Java, kein Golang, kein Groovy. Wir haben Deutsch. Deutsch als Programmiersprache. Und wenn wir eine Programmiersprache haben, müssen wir uns am Anfang einmal über die atomaren Einzelteile der Sprache verständigen.

Slide 5.3 (3:22) +51s
Das ist der Buchstabe, optimalerweise in UTF-8 kodiert. Das ist das Wort delimited durch zwei Leerzeichen. Der Satz wird in der Regel durch einen Punkt abgeschlossen, auch wenn es das Semikolon gibt. Aber so ist doch der Punkt, gefolgt von einem Leerzeichen meistens das Ende eines Satzes. Dann gibt es auch noch die Absätze, die fassen noch verschiedene Sätze zusammen. 
Darüber hinaus gibt es noch höhere Design-Strukturen, die werden im Vertragswesen oft nach dem D-K-A-T Prinzip strukturiert.

Slide 5.4 (3:49) +51s
D-K-A-T ist ziemlich simpel:

D steht für Dokument, und da haben wir gerade eins von. K sind die Kapitel, davon haben wir im Koalitionsvertrag neun Stück. Wir haben 22 Abschnitte innerhalb der Kapitel, und wir haben 200 Themen. Das mal so grob gezählt.

Slide 5.5 (4:08) +51s
Zählen das ist überhaupt noch so eine Sache. Wenn man etwas zählen möchte, um es nachher vielleicht auch zu vergleichen, sind Seitenzahlen und Zeilennummern nicht unbedingt zielführend, weil sie sind nicht Geräte-Unabhängig, sind sehr abhängig von Formatierungen. Sind ein bisschen aus der Zeit gefallen. Auch Lines-of-Code (LoC) nimmt man heutzutage gar nicht mehr in der Code-Metrik, weil auch die sich durch unterschiedliche Style-Guides unterschiedlich darstellen.
Buchstaben zählen wäre jetzt auch nicht so dolle, sehr inhaltsleer. Wörter sind da schon ein bisschen besser. Auch die bekommt man ohne Satzbau nur schwierig in einen Kontext, aber man kann die schonmal nach Nomen, Verben und Adjektive auseinanderdröseln. Man kann schonmal die Stopwörter daraus holen. Das ist schonmal keine schlechte Grundlage für Späteres.
Dann sind die Anzahl der Sätze das eigentliche wichtiges und am besten vergleichbare Mengengerüst. Das ist sehr gut vergleichbar mit dem sog. Non-Commenting-Sourcecode Statement, kurz NCSS. Das ist in den Code-Metriken die Basis-Einheit, wenn man nachher irgendwas messen will. Interessantes Slide-Deck dazu von Gerrit Beiner habe ich Euch hier verlinkt.
Was kann man denn nun mit diesen "Messereien" anstellen?

Slide 5.6 (5:19) +51s
Ich habe nun einfach mal die Wärter gezählt. Hab dann die gezählten Wörter mal in diese drei Klassen eingebaut: Nomen, Verben und Adjektive. Da sieht man mal was so die meist genutzen Nomen sind. Kein Wunder, dass "Deutschland", "Rahmen", "Ziel", "Land", "Mensch" die am größten gezeichneten Wörter sind. "Bund" und "Kommune" ist dann schon ein bisschen kleiner. "Zusammenarbeit" und "Digitalisierung" ist immer noch recht groß. Aber wenn ich da mal schauen mag ... Ich suche ... "Klima"... Ist wohl auch nicht ganz so groß, "Klimaschutz" dann auch noch ein bisschen kleiner. "Finanzierung" ist jetzt auch nicht so groß. Na gut, sind ja jetzt erstmal nur Nomen. 
Bei Verben ist es ein bisschen schwieriger. Verben an sich ... Kraft der Programmiersprache "Deutsch" kommen bestimmte Verben einfach öfters vor als andere. Deswegen sind Verben wir "müssen", "wollen", "setzen" und "schaffen" schon groß und das ist auch gut. Aber ob das jetzt eine Bewandtnis hat, dass "vereinfachen", "berücksichtigen" oder "anpassen" jetzt so klein sind, ist jetzt schwierig zu beurteilen, ohne weitere semantische Zuordnung.
Bei den Adjektiven sieht es schon ein bisschen anders aus. Das gibt schon ein bisschen Farbe in's Spiel. Da sehen wir "neu", "gut", "gemeinsam", "europäisch", "öffentlich". "digital" groß, wunderbar. Aber auch hier bin ich mir nicht ganz sicher, ob mich das verunsichern soll, das zum Beispiel "transparent" sehr klein ist, dass "ökologisch" mit zu den kleinsten gehört. Ahh gut, das sind nur Wortzählereien, dass muss erstmal noch nichts heißen. 
Ihr könnt Euch solche Wortwolken übrigens auch einfach selber bauen. Es gibt ein schönes Angebot unter wortwolken.com. Hab ich euch verlinkt. Schaut mal rein. Ist ganz nett.

Slide 5.7 (7:15) +51s
Was kann man noch machen, wenn man denn Wörter zählt? Man kann zu den Wertzahlen, auch Wortbeziehungen zählen. Also welche Wörter stehen nahe beieinander. "Europa" und "EU" das ist nun logisch. Aber zum Beispiel "National", "stärken", "Umsetzung" und "Schutz" sind Sachen, die hier zusammengefunden worden sind. Oder "führen", "schaffen", "Regelung" bilden hier ein kleines Cluster. Mit diesen Abständen kann man auch noch Graphen darstellen oder man kann das auch in sogenannte "Self-Organizing Maps" of Word darstellen. Da kommen wir später nochmal zu, wo wir das vielleicht verwenden könnten.

Slide 5.8 (7:58) +51s
Hier habe ich mal die Sätze gezählt. Die Sätze in den Abschnitten. Wir haben ja erfahren, wir haben 22 Absätze in den Kapiteln. Und da stellen wir fest: "Klimaschutz in einer sozial- ökologischen Marktwirtschaft" ist das größte Thema. Das freut mich. Aber vorsichtig, hier wird ja "Wirtschaft" und "Umweltschutz" zusammengefasst. Das macht es auch aus warum das denn so groß ist. Aber nichtsdestotrotz: Der Schwerpunkt stimmt, meines Erachtens. "Moderner Staat, digitaler Aufbruch", das ist das erste Kapitel. Ich glaube da hatten sie am meisten Spaß dran, denke auch da haben sich die drei Koalitionäre am ehesten gefunden. 
Aber da gibt es ein Kapitel, da steht nix. Finanzen, ausgerechnet das Thema. Keine Inhalte? Naja, ganz so isses nicht. Was hier nicht erfolgt ist, dass hier die Themen nicht nochmal zu Abschnitten zusammengefasst worden sind. Als ich das PDF-Dokument gesehen musste ich ein bisschen Schmunzeln. Ich habe geglaubt, dass haben sie absichtlich gemacht. Hätten sie bei Finanzen noch die Abschnitte ausformuliert, hätte das Inhaltsverzeichniss nicht mehr auf eine Seite gepasst. Ich weiß nicht, aber vielleicht auch nicht.

Slide 5.9 (9:09) +51s
Aber dann schauen wir doch mal in die Themen rein. Das sind jetzt die 200 Themen. Zugegeben, das wird jetzt im Video nicht so gut lesbar sein, deswegen habe ich hierzu mal was vorbereitet. Das Ganze habe ich mal als SVG rausgelassen. In dem SVG können wir mal ein bisschen besser rein und raus zoomen. Und da können wir uns unser beliebtes Thema "Marktwirtschaft" und "Umweltschutz" mal genauer anschauen. Das hier ist jetzt sortiert nach den Größen der Themenblöcke von links nach rechts und von oben nach unten werden die Themenblöcke immer kleiner. Das ist schonmal interessant: "Bahnverkehr" und "Autoverkehr" ist gar nicht so weit auseinander. Hätte mir jetzt ein bisschen mehr gewünscht für den Bahnverkehr, aber ok. "Tierschutz" ist gut. Aber "Radverkehr", schaut mal. Es gibt ganze zwei(!) Sätze zum Radverkehr. Hmm und "Fußverkehr" kommt gar nicht vor. 
Aber, wie wir hier sehen, bei Finanzen kommen dann ja doch ein paar Themen vor. Also ganz so schlimm ist es ja nicht. Sehr interessante Themen, wie Geldwäsche. Da war doch was: FIU. Neue CIA? Financial Intelligence Unit... Hey CyberCyber. Schauen wir mal was da noch passieren wird.
Aber so könnt ihr mal ein Überblick bekommen, wie die 200 Themen auf die Kapitel aufgeteilt sind.
Das SVG ist auch direkt in den Slides verlinkt, könnt ihr von da aus aufrufen.
Das schonmal so weit so gut zum Thema: Was kann ich denn so sehen, wenn ich einfach mal ein bisschen rumzähle und das gezählte darstelle.

Slide 6.1 (10:49) +51s
KV als Spezifikation.

Slide 6.2 (10:53) +51s
Hierzu mal ein kleines Gedankenexperiment.
Wie wäre denn das:
Das Volk wäre der „Customer“. Staat wäre ein „Lean Enterprise“. Ziele würden wir fürderhin als „Objectives“ bezeichnen und eine Legislaturperiode wäre ein „PI“, „Programm Increment“. Na? Kommt Euch das bekannt vor? Klingt doch ein bisschen wie SAFe? Ja tatsächlich das ist angelehnt an die SAFe-Methoden. Unten ist auch ein Link. Wenn Euch SAFe interessiert, könnte mal reinschauen. Es ist eine interessante Variante verschiedene Entwicklungs-Teams zusammen zu bringen. Es wird Vieles bereits Vorhandene kombiniert, wie Scrum, DevOps oder Release-On-Demand und solche Geschichten. Und es ist Customer, äh nein, Community-Driven. Ihr selbst könnt an diesem Framework mitarbeiten. Es wird auch versioniert. Es ist gerade Version 5.1 draußen. Das interessante ist, die Kollegen, die das weiterentwickeln, arbeiten selber auch nach SAFe. "Eat-Your-Own-Dogfood", find ich ganz interessant.
Aber wir wollen uns ja hier um den KV kümmern. Beim KV geht es ja hauptsächlich um Ziele. Das heißt die "Objectives" sind das erste Objekt der Begierde. Und hier hat SAFe ein ganz interessantes Regelangebot.


Slide 6.3 (12:20) +51s
Nach SAFe seien Objectives S.M.A.R.T. Das heißt 
- Specific (Ziele müssen eindeutig definiert sein)
- Measurable (Ziele müssen messbar sein)
- Achievable (Ziele müssen erreichbar sein) - Da fällt mir das mit dem Wahlalter ein. Wahlalter aber 16. Da bin ich auch stark dafür. Für Europa können sie das auch rein mit der Regierungsmehrheit machen, aber für den Bund brauchen sie eine 2/3 Mehrheit. Also das ist eigentlich aus sich selbst heraus gar nicht richtig erreichbar. 
- Reasonable (Ziele müssen angemessen sein)
- Time-bound (Ziele müssen in einem PI realisierbar sein). Das finde ich jetzt sehr, sehr interessant.

Slide 6.4 (13:05) +51s
Dazu habe ich mal folgendes gemacht. Ich habe mir mal alle Jahreszahlen aus dem Koalitionsvertrag rausgezählt und geschaut, wo liegen die denn. Da haben wir insgesamt 126 gefunden, die 6 vor und die 80 in der Legislaturperiode sind ok, sind fein, sind super. Da weiß ich genau, dass ist erreichbar in der Zeit, wo die Regierung in der Regierung ist. 
Aber es gibt auch 40 Ziele, die liegen außerhalb der Legislaturperiode. Das finde ich nur zweitranging optimal, denn im Zweifel sind die gar nicht erreichbar. Ok, fairer Punkt, man hat auch schon mal größere Ziele, man muss sich auch schonmal mit längeren Zeiträumen beschäftigen. Aber dann würde ich erwarten, dass zu diesen größeren Zielen etwas beschrieben steht, was in dieser Legislaturperiode getan werden wird, damit diese Ziele erreicht werden können. 

Slide 6.5 (13:59) +51s
Gut, dem wollte ich mal etwas genauer auf den Zahn fühlen. Das mit den 40 Zielen. Deshalb habe ich einen kleinen Zeitstrahl-Reader gebaut mit allen Jahreserwähnungen, die außerhalb der Legislaturperiode liegen. Wenn ihr nachher hier im Slide-Deck seid, und ihr könnt nicht direkt in der Folie darin lesen, dann ist hier nochmal ein Link zu einer HTML-Seite, da könnt ihr das außerhalb der Folie nochmal lesen. 
Ganz interessant hier gibt es auch ein positives Beispiel. 2026 möchten sie etwas erreichen, das liegt außerhalb der Legislaturperiode. Aber dafür möchten sie etwas tun, das liegt innerhalb der Legislaturperiode. So könnte man damit arbeiten. 
Gut ok, glaub dieses Wort hier [idealerweise] ist genug durch die Presse gegangen, da brauch ich hier jetzt nichts weiterzusagen.
Da seht ihr halt, da sind so einigen Sachen drin, für die ist nicht vorgesehen, dass sie innerhalb des PI's erreicht werden sollen. Somit wären ca. 40 Ziele nicht S.M.A.R.T.
So viel zu Thema: KV als Spezifikation.

Slide 7.1 (15:13) +51s
Kommen wir nun zum nächsten Thema: KV als Objektmodell.

Slide 7.2 (15:19) +51s
Äh Objektmodell? Da hat mich Koala-Kid öfters schräg zu angeguckt. 

Slide 7.3 (15:28) +51s
Sowas ist doch Architektur, das stinkt doch. Könnt ihr Euch vielleicht an die vielen Diskussion erinnern, die es in den Projekten gab:
Agil vs. Wasserfall
Anstand e.V. vs. Agile Manifesto
Big Upfront vs. Evolutionary Architecture
A0 Modell Tapetten vs. Kein Plan
Docs-as-Code, Desing-As-Code, Diagram-As-Code, 
Irgendwas-as-a-Service ... kennen wir doch nur zu genüge.
In diesem Spannungsfeld hat sich Simon Brown etwas interessantes ausgedacht. Und er hat es C4 genannt.
C4 hat jetzt nicht mit dem Kölner Chaos Club zu tuen. Es ist auch nicht der Sprengstoff mit gemeint. Mit C4 umschreibt Simon Braun einen Abstraktionslevel. Genauer genommen vier Abstraktionslevel. Das ist so ein bisschen ein "Missing Link" in der UML. In der UML haben wir zwar alle möglichen Arten von Artefakten, aber wir haben keine Idee, evt. mit Ausnahme bei SysML … Aber haben ansonsten keine Idee, wie wir größere Objektmodelle strukturieren, von dem Level-of-Details her betrachtet. Und da hat er sich gedacht, lass uns mal von vier Level ausgehen: 
Context.
Container. Damit sind jetzt nicht Docker-Container mit gemeint. Hier könnten wir von unseren Kapiteln und Abschnitte reden. 
Components. Das sind die Basic Packages. Das Thema. Und dann, der 
Code. Also der Text selbst.
In diesen vier Abstraktion Stufen denkt Simon Brown. 
Wer dazu was nachlesen möchte, dem habe ich hier unten was verlinkt. Auch über die Debatte Agile vs. Architecture. Ein schönes Slide-Deck, sehr amüsant zu lesen.
Ähem, ja gut. Toll. Und wie fängt da jetzt was an?

Slide 7.4 (17:19) +51s
Zum Beispiel Context. Könnte man mal so anfangen. Das wäre jetzt ein absolut minimalistische C4 Model auf System-Context-Ebene. Wir haben eine Regierung, wir haben einen Koalitionsvertrag und wir haben eine Beziehung: Die Regierung folgt unbedingt dem Koalitionsvertrag. Das könnte man jetzt weiter ausspinnen. Die Regierung besteht ja aus Ministerien, der Koalitionsvertrag besteht aus Kapiteln. Da gibt es bestimmte Sachzusammenhänge, die man da modellieren könnte. 

Slide 7.5 (17:47) +51s
Da habe ich ein kleines bisschen weiter angefangen.
Übrigens, kleine, interessante Nebeninformation: Plantuml wird öfters eingesetzt, um in Form von Diagram-as-Code, Architektur-Diagramme zu erstellen. Zu Plantuml gibt es auch ein C4-Plugin. Hier ist es verlinkt, könnt ihr es in github runterladen bzw. könnt ihr Euch diese Adresse, die jetzt hier nicht ganz zu sehen ist, entsprechend benutzen und dann könnt ihr ganz einfach mit plantuml C4-Modelle entwickeln.
Hier zum Beispiel ein paar Ministerien. Der Kalle ist unser Bundesgesundheitsminister, Herr Habeck gehört zum Wirtschaftsministerium und Herr Wissing ist im Verkehrsministerium. Alle wollen die Ziele des Koalitionsvertrages umsetzen und bilden zusammen das Kabinett.

Slide 7.6 (18:40) +51s
Im Bereich des Kontextes spricht Simon Brown auch von dem Vergleich zu Google Maps. Das man da zunächst die Kontinente hat, dann die Länder hat, dann die Städte und dann die Details sieht. Ich hatte schonmal von den Self-Organized Maps of Words gesprochen, die man mit KH Coder aus Texten generieren kann. Das könnte eventuell eine Idee sein, wie man in diese Richtung solche Wortlandkarten erzeugen könnte.

Slide 7.7 (19:15) +51s
Nun hat man nun seine Kontexte. Nun möchte man sein Objektmodell auch verwalten. Da gibt es einige Applikationen und Tools womit man Objektmodelle verwalten kann. Da gibt es unter anderem „StarUML“, „Visual Paradigm“ oder auch „Enterprise Architekt“. „Enterprise Architekt“ gefällt mir persönlich ganz gut. Da kann ich meine Strukturen drin verwalten. Aber Enterpise Architekt basiert auf UML, XMI ist das sperrige Austauschformat. XML mit seinen spitzen Klammern ist wohl nicht mehr ganz so modern. Ich weiß auch ehrlich gesagt auch nicht so richtig warum. Heutzutage macht man es doch viel lieber mit JSON oder YAML.
Das habe ich dann auch mal gemacht. Ich habe mir den Vertrag mal als JSON und YAML angeschaut. Bei dem ganzen Aufbereiten und Anschauen hab ich auch mal endlich wieder ein bisschen Python geschrieben. Habe ich schon lange nicht mehr gemacht. Macht richtig Spaß. Dabei habe ich mal neben ein vernünftiges Inhaltsverzeichnis vom Koalitionsvertrag generiert, den man dann auch besser lesen kann und wo man die Zusammenhänge besser erkennen kann. 
Dabei ist mir auch eine Idee gekommen: Wenn ich denn später alles in ein Objektmodell generiere und vorher dazu alles vom KV auseinander nehme, dann macht es vielleicht Sinn alle Teile, D-K-A-T orientiert, bibliographisch zu nummerieren. Dann weiß ich halt eben immer, wo ich gerade bin. Dann bin ich zum Beispiel hier bei T 2 1 1, der „Verwaltungsmodernisierung“. Damit weiß ich genau, wo ich gerade bin. Hier im Kapitel zwei, Abschnitt zwei, Thema 7. Oder T 2 2 7. Ich weiß sofort, ich bin in Kapitel zwei, Abschnitt zwei und Thema sieben. Damit habe ich immer einen Kontext, wo ich bin. Dann ist es auch egal, ob ich es auf A4 ausdrucke, ob ich es auf einem eBook-Reader lese oder sonst irgendwo.

Slide 7.8 (21:03) +51s
Ja gut und was habe ich nun davon ...

Slide 7.9 (21:12) +51s
... was habe ich von diesen OOM-Ideen?
Naja zum Beispiel könnte man die ganzen D-K-A-T Elemente annotieren. Wir hatten von S.M.A.R.T. gesprochen, bei den Spezifikationen. Ihr erinnert Euch noch? Da könnte man ein Ranking einführen. Akzeptanzkriterien könnte man noch ergänzen. Man könnte auch ganz einfach die Minister zuordnen, denen ein Icon geben. Oder die Farben der Parteien verwenden. Oder ganz einfach mal die Lesedauer dranschreiben. Man könnte die D-K-A-T Elemente auch noch untereinander miteinander verknüpfen und die Beziehungen dazu aufbauen. Oder halt auch Literatur- und Code-Metriken anreichern. Das wäre so eine Idee was man recht gut machen könnte.
Aber man könnte auch dann die Entitäten, die man so auseinander genommen hat mit Generatoren in neue Kontexte generieren.

Slide 7.10 (22:02) +51s
Oder habt ihr vielleicht noch eine Idee? Jetzt kommt Deine Idee. Was könnte man so machen, wenn man solche Texte in Objektmodelle hätte? Was könnte man damit anstellen? Ich sprach eben von S.M.A.R.T.-Ranking bei den Objectives, das sind ja so die Ziele. Im PI gibt es da ein Verfahren, das man den einzelnen Zielen sogenannte BV annotiert, also Business-Values. Das heißt der Stakeholder sagt was bringt mir dieses Ziel an Wert, wenn ich dieses Ziel umsetze. Im Vorfeld, bevor es umgesetzt worden ist, wird das annotiert. Nach einem Inkrement, einer Kadenz, kommt derjenige der es vorher annotiert dazu und sagt, was denn jetzt nun der "Actual Value" (AV) ist. Was hat mir das denn nun wirklich gebracht. Wäre vielleicht eine interessante Methode die Zielerreichung einer Legislaturperiode auseinander zu nehmen.
Oder etwas zu genieren. Workadventure Spaceships aller Arten und Sorten, Museen verschiedener Arten und Sorten. Und da habe ich etwas Interessantes gefunden, das könnt ihr direkt im Anschluss an diesen Talk anhören: The Everything Exhibition. Das nimmt sich verschiedene Strukturen. Aktuell basiert es auf Wiki-Strukturen, Wikipedia-Strukturen, wenn ich mich nicht täusche und generiert daraus 3D-Ausstellungsräume. Das finde ich sehr spannend, das gucke ich mir auf jeden Fall im Anschluss dann auch selber an.
Ja, Ihr seht für das Jahr 2022 habe ich ein neues Hobby. Ich denke da wird noch das ein oder andere zu rumkommen.
So viel zum Thema KV als Objektmodell.

Slide 8.1 (23:54) +51s
Kommen wir nun zum letzten größeren Kapitel dieses kleinen Vortrags: KV als Literatur. 

Slide 8.2 (24:01) +51s
Der Koalitionsvertrag ist ja auch irgendwie ein Text, Fachliteratur zugegebener weise, aber möchte ja doch eigentlich auch gelesen werden. Und da hatte ich so am Anfang so meine Probleme mit. Ich wusste auch nicht so richtig warum ... Aber als ich das PDF so in der Hand hatte, ist es mir einfach schwer gefallen einfach so loszulesen. Ich hatte zwischendurch sogar das Gefühl: wollen die das vielleicht auch einfach nicht. Wollen die das vielleicht auch lieber geheim halten, wie sie ja doch so vieles geheim gehalten haben?


Slide 8.3 (24:32) +51s
Dann habe ich mir mal das Dokumentenlayout selbst angeschaut. Dabei sind mir so ein paar Sachen aufgefallen, die mir so gar nicht gut gefallen haben. Zum einen mal der Zeilenabstand von 1,5. Bläht das alles wahnsinnig auf und macht das dann wirklich ein bisschen schwieriger zu lesen. Die Seitenränder, ok oder nicht, sind halt auch bisschen groß. Die Absatzabstände auch n bisschen viel. Aber richtig Krass finde ich Blocksatz zum Lesen. Blocksatz auf A4 auch noch, so ewig lange Zeilen. Da fällt es einem wirklich schwierig zu lesen, wenn man es mal entspannt machen möchte. 
Eine andere, eher Kleinigkeit dagegen, ist die Themen-Nummerierung, bzw. die Nicht-Nummerierung. Da fällt es einem schwer, wenn man sich mal linear in den Text reinfallen lässt und auf Thema stößt ... Wo bin ich denn gerade? Zum Beispiel Fachkräfte. Bin ich da bei „Internationales“, bin ich da bei „Wirtschaft“ oder in der „Arbeitswelt“ ... Ich weiß es oft nicht. Deshalb würde ich vorschlagen auch die Thema-Ebene durchzunummerieren.  

Slide 8.4 (25:36) +51s
Aber schaut mal, das ist wirklich, echt aufgeblasen. Das hier sind jetzt knapp 177 Seiten. 

Slide 8.5 (25:41) +51s
Wenn man doch einfach nur die Seitenränder auf Normal, den Absatzabstand auf Normal und den Zeilenabstand auf eins setzt. Linksbündig statt Blocksatz. Ok, linksbündig verringert jetzt nicht die Seitenmenge, aber erhöht ein bisschen die Lesefähigkeit. Dann komme ich mal ganz locker von 177 auf 101 Seiten. Das klingt zunächst vielleicht mal kleinkariert, und solange man es nicht ausdruckt, ist es ja auch egal. 

Slide 8.6 (26:11) +51s
Aber, stellen wir uns mal vor, der Vertrag würde 1000x ausgedruckt. In den Regionalbereichen, in den Ortsverbänden, in den Redaktionen ... In den Ortsverbänden aller 4,5,6 Parteien... Interessierte Menschen gibt es bestimmt auch zu genüge. Ich bin mir gar nicht sicher, ob 1000 wirklich zu hoch gegriffen ist. Aber gehen wir mal von 1000 aus. Dann wären 76.000 Seiten einfach überflüssig. Wir hätten 43% Papier, Wasser und Co2 verschwendet für Nuppes. Das finde ich ein bisschen Schade.
Warum denn nicht gleich fortschrittlich, digital, klima- & energie schonend Formate anzubieten, die zudem auch noch geräteunabhängig sind? 
Aber ich will nicht nur einfach motzen, ich will auch was tun.

Slide 8.7 (27:00) +51s
Ich habe den Koalitionsvertrag in verschiedenen Varianten mal ausgeprägt. Ich habe mal eine Variante gebaut in HTML. Wie ich sie mir gerne vorstellen würde, wie ich ihn gerne lesen würde, sogar auch auf dem Handy möglich. Wunderschön - Für mich persönlich, mein Geschmack. Dann habe ich Euch auch mal eine ePub Version zur Verfügung gestellt, die könnt ihr auf Eurem eBook-Reader lesen. Habe das Ganze auch mal als Markdown rausgelassen. Wunderschön in Gitlab oder Github. Oder auch, wie ich es eben gezeigt habe, den Vertrag mal in alle Themen atomisiert. Mit den Einleitungstexten insgesamt 231 Textdateien erzeugt. Die könnt ihr Euch hier die Zip-Datei herunterladen und auf der Konsole auspacken und wunderschön in jeder Shell lesen. Mit dem tree Befehl könnt ihr auch prima da durch navigieren. Es ist eine interessante Erfahrung den Koalitionsvertrag auf der Shell zu lesen. Geht ganz gut. Wenn ihr das Lesen wollt, dann hab ich Euch noch was zusammengestellt.

Slide 8.8 (27:51) +51s
Ich habe Euch mal die Lesedauer mit Hilfe von WORTLIGA ermittelt. Das bietet an ... und gibt eine Abschätzung an, wie lange ein Text braucht um gelesen zu werden. Damit habe ich Euch mal eine Playlist zusammengestellt der verschiedenen Kapitel. Es muss nicht immer alles von Anfang bis Ende gelesen werden. Man kann sich auch explorativ das eine oder andere heraussuchen. Und dann sieht man hier dabei, wie lange man braucht, wenn man das selber liest.  
Interessant ist die Idee von Birte Schneider. Sie hat den ganzen Koalitionsvertrag am Stück vorgelesen: 8:50:00 Das könnt ihr Euch auf Youtube anschauen. Habe ich hier unten verlinkt. 
Dann vielen Lieben dank, das war's dann aber erstmal so weit so gut.

Slide 9.1 (28:37) +51s
One Last Thing ... Keine Session ohne One Last Thing.

Slide 9.2 (28:45) +51s
Also, ich habe ja jetzt das ein oder andere an Spitze ausgeteilt. Aber wenn man sich den Vertrag jetzt nun mal mit einer rosaroten Brille anschaut, und den Happy-Flow an den Sachen sieht, dann gibt es doch eine Menge Sachen, die garnicht mal so schlecht sind. 
Abschaffung von §219 find ich toll. Wahlalter ab 16 find ich auch super, auch wenn wir wissen, dass das mit dem Bund nicht so einfach wird. Cannabis? Klasse Sache. EEG-Umlage: Endlich kommt das Weg. Die DB-Netz wird "Gemeinwohlorientiert". Ich bin gespannt, wie sich das ausprägt. „Atomenergie ... kommen selber für ihre Kosten auf.“ Hallo Macron, nix Greenwashing für Atom, nix Greenwashing für Fukushima, Harrisburg oder Tschernobyl. Ne komm, das ist doch mit das Schlimmste, was jemals auf der Welt erfunden worden ist. 
Bleibe- und Chancen-Aufenthaltsrecht find ich auch eine Supersache. Endlich fasst das mal jemand an. Endlich ermöglicht jemand mal den Menschen, teilweise am Rand unserer Gesellschaft den Eintritt in unsere Gesellschaft, wo sie sonst doch nur noch die Kriminalität als Ausweg gesehen haben. Rechtsanspruch auf Open Data. Fantastisch! FragDenStaat wird sich freuen, und ich bin mal sehr, sehr neugierig, ob denn dann mal die Deutschen DIN-Normen öffentlich und frei zur Verfügung stehen werden. Wäre ja mal interessant. Recht auf Anonymität? Jippieh, ich kann für Euch der CodeFreezR bleiben. OpenSource und MultiCloud für öffentliche Aufträge? Ganz, ganz fantastisch. 
Teilweise freue ich mich auf die Sachen, die da drinstehen. Wenn ihr dazu noch mehr lesen wollt. Speziell was auch unsere "Cyber"-Angelegenheiten anbelangt, schaut mal bei Netzpolitik.org nach. Da habe ich einen schönen Artikel gefunden, den habe ich Euch hier verlinkt. Da haben viele dran mitgearbeitet. Sehr, sehr lesenswert.
Aber, aber, aber, aber nix ohne aber...


Slide 9.3 (30:27) +51s
Das hier sind Themen und Wörter, die nicht im Koalitionsvertrag auftauchen. Die Größe der Wörter habe ich mal grob mit der Anzahl der Google-Findings abgeglichen. Interessant: „Fahrrad“ ist mit das Größte Finding, ok, gut. Aber es wird nicht über Fraking gesprochen. Tempolimit … Ah Moment Tempolimit. Da kommt doch jetzt bestimmt einer um's Eck und behauptet: Tempolimit steht doch drin. Stimmt, steht auch drin: „Ein generelles Tempolimit wird es nicht geben.“
Ja, und da denke ich mir, da müssen wir n bissel aufpassen. Im Gegensatz zu vielen anderen Regierungen ... das waren alles nicht so meine Regierungen. War halt die Mehrheit, ich war nicht Mehrheit. Aber jetzt, geht's Euch vielleicht so wie mir ... Ich habe eine von den drei Parteien gewählt. Jetzt fühle ich mich dafür mitverantwortlich. Deswegen möchte ich schon schauen, dass wir den Koalitionsvertrag nehmen, als das was er ist. Er ist ein Versprechen, er ist keine Gesetzesgrundlage. Man kann an ihm, und man muss an ihm noch einiges Feilen ... und müssen aufpassen, dass das gut wird. 
Jetzt bin ich Thematisch durch, jetzt bleibt mir nur noch eines übrig.

Slide 10.1 (31:40) +51s
Ein "One More Last Thing" ... Eine Danksagung

Slide 10.2 (31:44) +51s
… an Bleeptrack zum Beispiel für Koala-Kid. Da habe ich mich super dolle drüber gefreut. Habt ihr vielleicht schon erkannt, am Zeichenstil.
… an die Remote-Rhein-Ruhr-Stage (r3s) für die Bühne. Super Cyber. Best Cyber ever. Habt ihr super gemacht. Ich war super happy, dass ihr das angenommen habt. 
… und natürlich an meine Familie. Sie hat sehr viel Geduld aufgebracht, hat mir so einiges an Feedback gegeben, mich teilweise auch angespornt, bisschen an Weihnachten auf mich verzichtet. Vielen lieben Dank dafür.
Dann noch natürlich, wie könnt ihr die Sachen erreichen? Hier die Projekt-URL, ein Trello, wo wir planen, was wir nachher verpeilen ^^. Wir haben die Folien. Ich versuche sie ... Mit ein bisschen Glück habe ich sie während des Vortrags hochgeladen. Dann könnt ihr sie auf der Pretalx-Seite direkt herunterladen.
Erreichen selbst könnt ihr mich im Rocket-Chat unter CodeFreezR. Ihr könnt mir eine E-Mail schreiben, ihr könnt mich im Fediverse antreffen. Nicht selten bin ich aber auch in Slack zu erreichen, bei den Kollegen von Cloud-Native. 
Das war's denn aber jetzt endgültig. Vielen lieben Dank für Eure Aufmerksamkeit. Bis nächste Mal. CiaoTschüss.

34:10 Herald: Auch von unserer Seite: Vielen, vielen lieben Dank für den Talk. Der war superspannend und ich glaube da können sich einige Zuschauer mit anschließen. Ich hätte nicht gedacht, allein daß es so viel Ersparnis gibt und es gibt auch einiges an Fragen.  Deswegen hauhe ich einfach mal die erste Frage raus.
34:31 Frage, Herald:  Was war die Datenbasis für die gezeigten Sehenswürdigkeiten, um zum Ranking der Inhalte zu kommen. 
34:37 Antwort, CodeFreezR: Grundlage für das gezeigter war der Koalitionsvertrag mit all seinen Kapiteln, Absätzen, Texten und Formulierungen. Das habe ich alles aus dem Koalitionsvertrag heraus generiert.
34:45 Frage, Herald: Dann die nächste Frage. Könnten man auch die Parteiprogramme der Ampel linken, dann wüsste man wer welche Teile einbringen könnte?
35:05 Ja, das ist interessant. Also die Vergleichsanalyse von den Wahlprogrammen zu dem Koalitionsvertrag, will ich auf jedenfall noch in den nächsten Wochen oder Monaten dezidiert angehen. Da will ich nämlich auch wissen, wer hat denn was wie beeinflusst. Aber die Wahlprogramme selbst, kann ich mal raussuchen. Wer das gerne möchte, nimmt Kontakt mit mir auf, dem stell ich das zur Verfügung.
35:34 Herald: Das ist superlieb, dann direkt zur nächsten Frage: Ist der Koalitionsvertrag wirklich so ernst gemeint so gelesen zu werden? Ist es nicht eine reine Fensterrede, oder gar viel mehr gar nicht das Ergebnis das Ziel, sondern der Weg?  Also das die Ampelparteien Arbeitsgruppen bilden und durch Zwang zum Team-Building gezwungen werden? Also analog zum dem was beim Selbst-Bildnis, Mission-Statements in Firmen gemacht werden soll?
36:05 Antwort, CodeFreezR: Das war jetzt eine lange, sehr lange Frage. 
36:08: Herald: Also ich glaub zusammengefasst, ob es nicht eher nur viel gerede ist und mehr zum Team-Building und bilden ein Selbstbildnis bilden sollte.
36:18: Ja unbedingt, das würde ich auf jedenfall unterschreiben. Vor allem Kapitel 1. Das war/ist so richtig eine Selbstfindungsabteilung. Aber sie müssen sich ja auch irgendwie zusammenfinden. Sie sind jetzt auch nicht unbedingt von vornherein direkt beieinander. Und von daher finde ich es auch gar nicht mal so verkehrt, daß man sich da in einem Koalitionsvertrag erstmal findet. Das ist ja auch eine Absichtserklärung. Sie wollen das auch so machen. Die haben das jetzt nicht nur so geschrieben, um dann Ruhe vor uns zu haben. Sondern da sollten wir schauen, daß sie das auch wirklich so machen. Und dass sie hier nicht nur Dumm rum geredet haben.
36:55 Herald: Dann noch eine Frage, kann man via git und Versionierung an einer Erweirtung und Updates in fünf Jahren arbeiten?
37:05 CodeFreezR: Klar warum nicht? Ich habe sogar vor, nicht nur die Wahlprogramm mit in's git rein zu nehmen, sondern auch die Koalitionsverträge von den Legislaturperioden davor, um die auch zu vergleichen. Also da werde ich regelmäßig was updaten, ja.
37:22 Herald: Dann habe ich einfach noch eine Frage: Hat dir die Arbeit daran Spaß gemacht, oder wurde es einfach nur noch anstrengend?
37:28 Das hat unbedingt Spaß gemacht. Also ich habe auch eine ganze Menge dabei gelernt, und wie ihr seht, ich hab mir ja auch für dieses Jahr 2022 einiges vorgenommen da noch mehr zu machen. Stay tuned. Ja ich mach da bestimmt noch weiter. Ich würde mich auch tierisch freuen, wenn da noch wer anderes mit Lust hat.
37:50 Sehr cool. Das war ein sehr, sehr schönes Schlusswort. Also wenn ihr Lust habt mit dran zu arbeiten, meldet euch bei CodeFreezR; und noch einmal vielen, vielen lieben Dank für diesen supercoolen Talk.
38:02 Outro-Musik
 

