
    tbd. Hier schonmal die Frage aus dem Cyber vom Vortrag auf der rc3.rev @r3s:
    
* Was war die Datenbasis für die gezeigten Sehenswürdigkeiten um zum Ranking der Inhalte zu kommen?
* Gibt es einen Pointer zum Source des Koalitionsvertrages (möglichst als strukturierter Text)? (Beantwortet im Talk)
* Könnte man nicht auch die Parteiprogramme der Ampel linken? Dann wüsste man, wer welche Teile letztlich einbringen konnte.
* Ist der KV wirklich so ernst gemeint, so gelesen zu werden? Ist es nicht eine reine Fensterrede? Oder gar vielmehr gar nicht das Ergebnis (KV) das Ziel, sondern der Weg: Also dass die Ampel-Parteien Arbeitsgruppen bilden und durch den Zwang (einen KV produzieren zu müssen) zum Teambuilding gezwungen werden? Also analog zu dem was bei Selbstbildnis"/“Mission Statement” in Firmen gemacht werden soll.
* kann man via Versionierung (git) an einer Erweiterung und Updates in 5 Jahren arbeiten?
