import requests
from bs4 import BeautifulSoup

# import shutil
import os
# shutil.move("M://source/folder", "M://destination/folder") 
# os.rename("M://source/folder", "M://destination/folder")

tmp_path = "D://dbt/tmp//"

with open("KV-Bund-2021.html", encoding="UTF-8") as fp:
    soup = BeautifulSoup(fp, 'html.parser')

h1_path = ""
h2_path = ""
h3_path = ""
h2_only = ""

from_path_h1 = ""
to_path_h1 = ""

from_path_h2 = ""
to_path_h2 = ""

from_path_h3 = ""
to_path_h3 = ""

output = ""

h1 = 0
h2 = 0
h3 = 0

intro1 = ""
intro2 = ""
intro3 = ""
for tag in soup.find_all(['h1','h2','h3']):
    if tag.name == "h3":
        h3 = h3 + 1
        toc_h3 = "T " + str(h1) + " " + str(h2) + " " + str(h3) + " "
        intro3 = intro3 + h1_path + h2_only + tag.text + ".txt"
        #print (toc_h3 + "\t\t" +  intro3)

        from_path_h3 = to_path_h1 + "//" + toc_h2 + h2_only + tag.text + ".txt"
        to_path_h3 = to_path_h1 + "//" + toc_h2 + h2_only + toc_h3 + tag.text + ".txt"

        print ("i3: " + to_path_h3)
        #os.rename(from_path_h3, to_path_h3)

        intro3 = ""
        
    elif tag.name == "h2":
        h2 = h2 + 1
        toc_h2 = "A " + str(h1) + " " + str(h2) + " "
        h2_only = tag.text + "//"
        h2_path = h1_path + tag.text + "//"
        
        from_path_h2 = to_path_h1 + "//" + tag.text
        to_path_h2 = to_path_h1 + "//" + toc_h2 + tag.text
        
        #os.rename(from_path_h2, to_path_h2)
        intro2 = to_path_h2 + "Einleitung.txt"
        print ("i2: " + intro2)
        
        #print (toc_h2 + "\t\t" + intro2)
        h3 = 0
        
    else:
        h1 = h1 + 1
        toc_h1 = "K " + str(h1) + " "
        h1_only = "root//bund//2021//" + tag.text
        h1_new = "root//bund//2021//" + toc_h1 + tag.text
        h1_path = h1_only + "//"
        intro1 = h1_path + "Einleitung.txt"
        #print (toc_h1 + "\t\t" +  intro1)
        #renaming = h1_only + " to " + h1_new
        
        from_path_h1 = tmp_path + h1_only
        to_path_h1 = tmp_path + h1_new
        #print (fto_path_h1)
        #os.rename(from_path_h1, to_path_h1)
        intro1 = to_path_h1 + "Einleitung.txt"
        print ("i1: " + intro1)

        h2_only = ""
        to_path_h2 = ""
        toc_h2 = ""
        h3 = 0
        h2 = 0
        

#print(output) tag.next_sibling
#with open("root//bund//2021//toc.txt", 'w', encoding='utf-8') as file:
#    file.write(output)
