
Wir wollen das bauliche Kulturerbe nachhaltig sichern, zugänglich machen und das Denkmalschutzsonderprogramm unter ökologischen Aspekten weiterentwickeln. Wir schaffen eine „Bundesstiftung industrielles Welterbe“ und prüfen europäische Mechanismen zur Förderung des Denkmalschutzes.
Wir setzen den Reformprozess der Stiftung Preußischer Kulturbesitz gemeinsam mit den Ländern fort. Ein erhöhter Finanzierungsbeitrag des Bundes hat die grundlegende Verbesserung der Governance zur Voraussetzung. Wir entwickeln das Humboldt Forum als Ort der demokratischen, weltoffenen Debatte.

