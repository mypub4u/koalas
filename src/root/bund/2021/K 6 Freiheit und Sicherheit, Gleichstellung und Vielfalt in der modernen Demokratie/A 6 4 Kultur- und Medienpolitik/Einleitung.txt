
Wir wollen Kultur mit allen ermöglichen, indem wir ihre Vielfalt und Freiheit sichern, unabhängig von Organisations- oder Ausdrucksform, von Klassik bis Comic, von Plattdeutsch bis Plattenladen. Wir sind überzeugt: Kulturelle und künstlerische Impulse können den Aufbruch unserer Gesellschaft befördern, sie inspirieren und schaffen öffentliche Debattenräume.
Wir setzen uns für eine starke Kulturszene und Kreativwirtschaft ein. Wir stehen für eine diskriminierungsfreie Kultur- und Medienpolitik.
Wir wollen Kultur in ihrer Vielfalt als Staatsziel verankern und treten für Barrierefreiheit, Diversität, Geschlechtergerechtigkeit und Nachhaltigkeit ein.

