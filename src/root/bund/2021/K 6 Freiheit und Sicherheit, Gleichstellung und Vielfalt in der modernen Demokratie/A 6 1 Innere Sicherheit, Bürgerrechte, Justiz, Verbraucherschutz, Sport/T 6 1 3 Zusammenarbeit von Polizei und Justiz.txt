
Wir intensivieren die grenzüberschreitende polizeiliche und justizielle Zusammenarbeit rechtsstaatlich, sichern dabei hohe Datenschutzstandards und verbessern den grenzüberschreitenden Rechtsschutz. Wir streben die Weiterentwicklung von Europol zu einem Europäischen Kriminalamt mit eigenen operativen Möglichkeiten an. Die Europäische Staatsanwaltschaft wollen wir finanziell und personell ausbauen.
Gemeinsamt mit den Ländern wollen wir die Sicherheitsarchitektur in Deutschland einer Gesamtbetrachtung unterziehen und die Zusammenarbeit der Institutionen für die Sicherheit der Menschen effektiver und wirksamer gestalten.
Wir wollen mit den Ländern die Aussagekraft der Kriminal- und Strafrechtspflegestatistiken nachhaltig verbessern. Wir verankern den periodischen Sicherheitsbericht gesetzlich.
Wir verstetigen mit den Ländern den Pakt für den Rechtsstaat und erweitern ihn um einen Digitalpakt für die Justiz.

