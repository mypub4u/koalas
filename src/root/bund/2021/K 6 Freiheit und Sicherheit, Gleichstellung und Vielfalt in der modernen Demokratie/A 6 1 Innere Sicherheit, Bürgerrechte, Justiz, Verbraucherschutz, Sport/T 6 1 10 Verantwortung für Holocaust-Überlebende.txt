
Deutschland stellt sich seiner historischen Verantwortung für die Überlebenden des Holocaust. Wir werden die laufenden Entschädigungsleistungen wie auch die finanzielle Unterstützung für die Pflege der heute hoch betagten Holocaust-Überlebenden konsequent sicherstellen, um ihnen ein Leben in Würde zu ermöglichen.
Gleichzeitig sollen die Zukunftsaufgaben der Wiedergutmachung nationalsozialistischen Unrechts besondere Sichtbarkeit erlangen. Hierzu gehören insbesondere der Aufbau einer zentralen digitalen Themenplattform zur Wiedergutmachung von NS-Unrecht und die Verstärkung und dauerhafte Förderung von Holocaust Education.

