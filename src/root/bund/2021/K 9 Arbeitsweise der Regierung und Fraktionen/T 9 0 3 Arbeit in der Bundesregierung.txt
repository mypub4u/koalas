
Im Kabinett werden Entscheidungen einvernehmlich getroffen, kein Koalitionspartner wird überstimmt.
In allen vom Kabinett beschickten Gremien, Beiräten und Ausschüssen sind die Koalitionsfraktionen nach ihren Kräfteverhältnissen vertreten. Die Besetzung erfolgt im gegenseitigen Einvernehmen. Grundsätzlich sind alle Koalitionspartner vertreten, sofern es die Anzahl der Vertreterinnen bzw. Vertreter des Bundes zulässt.
Zur ressortübergreifenden Koordinierung besonderer Ziele des Koalitionsvertrages können Kabinettsausschüsse gebildet werden, deren Aufgaben und Mitglieder einvernehmlich zwischen den Koalitionspartnern festgelegt werden.
Ressort- und Kollegialitätsprinzip bleiben weiterhin grundlegend für das Regierungshandeln.
Die Koalitionspartner werden in der Bundesregierung eine gemeinsame Arbeits- und Umsetzungsplanung der Vorhaben dieses Vertrages erarbeiten und fortlaufend aktualisieren. Die Bundesregierung wird die Umsetzung der Vereinbarung laufend dokumentieren.
Die Koalitionspartner vereinbaren, die Geschäftsordnung der Bundesregierung zu modernisieren und den veränderten nationalen wie internationalen Gepflogenheiten mit Blick auf die Effizienz des Regierungshandelns anzupassen.
Die Koalitionspartner treffen in der Bundesregierung Personalentscheidungen einvernehmlich. Dies gilt auch für Personalvorschläge der Bundesregierung bei internationalen Organisationen und bei EUInstitutionen. Bei übergeordneten Personalfragen streben die Koalitionspartner eine insgesamt ausgewogene Lösung an, die allen drei Partnern gerecht wird und eine faire Verteilung abbildet. Für Personalfragen in den obersten Bundesbehörden selbst und nachgeordneten Behörden sowie mittelbaren und unmittelbaren Bundesbeteiligungen gilt das Ressortprinzip, soweit nichts anderes vereinbart ist.

