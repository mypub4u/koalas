
Der stationäre Handel in Deutschland braucht attraktive Rahmenbedingungen, um im Strukturwandel gegenüber dem reinen Online-Handel bestehen und von der Digitalisierung profitieren zu können. Wir bemühen uns weiter um fairen Wettbewerb zwischen Geschäftsmodellen digitaler Großunternehmen und den lokal verwurzelten Unternehmen. Wir wollen die digital gestützte Wertschöpfung in Industrie, Handel, Handwerk und Dienstleistung unterstützen und dafür ein Level Playing Field herstellen.
Wir nutzen das Förderprogramm „Zukunftsfähige Innenstädte und Zentren“ und führen die Innenstadtstrategie des Bundes fort, insbesondere das Programm „Lebendige Zentren“ im Rahmen der Bund-Länder-Städtebauförderung. Sie sollen für eine Verbesserung der Aufenthalts- und Erlebnisqualität in den Innenstädten genutzt werden.
Wir werden die konkreten Rückzahlmodalitäten der Corona-Hilfen prüfen.

