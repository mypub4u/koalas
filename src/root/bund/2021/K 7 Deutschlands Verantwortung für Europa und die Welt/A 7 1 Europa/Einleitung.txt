
Eine demokratisch gefestigtere, handlungsfähigere und strategisch souveränere Europäische Union ist die Grundlage für unseren Frieden, Wohlstand und Freiheit. In diesem Rahmen bewältigen wir die großen Herausforderungen unserer Zeit wie Klimawandel, Digitalisierung und Bewahrung der Demokratie. Eine solche EU bleibt einer multilateralen und regelbasierten Weltordnung verpflichtet und orientiert sich an den Nachhaltigkeitszielen der Vereinten Nationen (SDG).
Wir setzen uns ein für eine EU, die ihre Werte und ihre Rechtsstaatlichkeit nach innen wie außen schützt und entschlossen für sie eintritt. Als größter Mitgliedstaat werden wir unsere besondere Verantwortung in einem dienenden Verständnis für die EU als Ganzes wahrnehmen.

