
Die Flexi-Rente wollen wir durch bessere Beratung in ihrer Bekanntheit verbreitern und die Regelung zum Hinzuverdienst bei vorzeitigem Rentenbezug entfristen. Gemeinsam mit den Sozialpartnern werden wir in einen gesellschaftlichen Dialogprozess darüber eintreten, wie Wünsche nach einem längeren Verbleib im Arbeitsleben einfacher verwirklicht werden können und dabei insbesondere einen flexiblen Renteneintritt nach skandinavischem Vorbild und die Situation besonders belasteter Berufsgruppen in die Diskussion mit einbeziehen.

