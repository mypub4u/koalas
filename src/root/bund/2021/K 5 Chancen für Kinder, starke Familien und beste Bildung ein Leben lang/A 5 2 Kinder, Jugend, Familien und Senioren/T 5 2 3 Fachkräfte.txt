
Gemeinsam mit den Ländern und allen relevanten Akteuren entwickeln wir eine Gesamtstrategie, um den Fachkräftebedarf für Erziehungsberufe zu sichern und streben einen bundeseinheitlichen Rahmen für die Ausbildung an. Sie soll vergütet und generell schulgeldfrei sein.
Mit hochwertigen Qualitätsstandards in der Kindertagesbetreuung, sorgen wir für attraktive Arbeitsbedingungen. Wir wollen die praxisintegrierte Ausbildung ausbauen, horizontale und vertikale Karrierewege sowie hochwertige Fortbildungsmaßnahmen fördern und Quereinstieg erleichtern.
Umschulungen werden wir auch im dritten Ausbildungsjahr vollständig fördern.

