import requests
from bs4 import BeautifulSoup

with open("KV-Bund-2021.html", encoding="UTF-8") as fp:
    soup = BeautifulSoup(fp, 'html.parser')

h1 = 0
h2 = 0
h3 = 0
i = 1
output = ""
for tag in soup.find_all(['h1','h2','h3']):
    if tag.name == "h3":
        h3 = h3 + 1
        toc_h3 = "    " + str(h1) + "." + str(h2) + "." + str(h3) + " " + tag.text
        output = output + toc_h3 + "\n"
    elif tag.name == "h2":
        h2 = h2 + 1
        toc_h2 = "  " + str(h1) + "." + str(h2) + " " + tag.text
        output = output + toc_h2 + "\n" 
        h3 = 0
    else:    
        h1 = h1 + 1
        toc_h1 = str(h1) + "." + " " + tag.text 
        output = output + toc_h1 + "\n"
        h2 = 0
        h3 = 0

print(output)
with open("root//bund//2021//toc.txt", 'w', encoding='utf-8') as file:
    file.write(output)
