show tables;

CREATE TABLE h1_titles (
    h1_id    INTEGER      PRIMARY KEY,
    h1_title VARCHAR (250) 
)


CREATE TABLE h2_titles (
    h2_id    INTEGER      PRIMARY KEY,
    h2_title VARCHAR (250) 
);


CREATE TABLE h3_titles (
    h3_id    INTEGER      PRIMARY KEY,
    h3_title VARCHAR (250) 
);


CREATE TABLE h2_count (
    h2_base_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    h1_id INTEGER, 
    h2_id INTEGER,
    sentences INTEGER
);

CREATE TABLE h3_count (
    h3_base_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    h1_id INTEGER, 
    h2_id INTEGER,
    h3_id INTEGER,
    sentences INTEGER
);



INSERT INTO h1_titles (h1_id, h1_title) VALUES 
(1, 'Präambel');

INSERT INTO h2_titles (h2_id, h2_title) 
VALUES 
	(2, 'Moderner Staat, digitaler Aufbruch und Innovationen'),
	(3, 'Klimaschutz in einer sozial-ökologischen Marktwirtschaft'),
	(4, 'Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt');

INSERT INTO h3_titles (h3_id, h3_title) VALUES 



-----------------------------------------------------------------------------------------------
*** Sätze pro Kapitel
SELECT h1_id, COUNT(*) as sentences
FROM bun
GROUP BY h1_id

**** Mit Titel
SELECT bun.h1_id, COUNT(*) as sentences, h1_titles.h1_title as title
FROM bun
JOIN h1_titles ON bun.h1_id = h1_titles.h1_id
GROUP BY bun.h1_id

-----------------------------------------------------------------------------------------------
*** Sätze pro Abschnitt
SELECT h1_id, h2_id, COUNT(*) as sentences
FROM bun
WHERE h2_id > 0
GROUP BY h1_id, h2_id

**** H2 Counter befüllen:
INSERT INTO h2_count (h1_id, h2_id, sentences) 
SELECT h1_id, h2_id, COUNT(*) as sentences
FROM bun
WHERE h2_id > 0
GROUP BY h1_id, h2_id

*** Mit Titel
SELECT h2_count.h2_base_id as id, h2_count.h1_id as h1, h2_count.h2_id as h2, h2_count.sentences, h2_titles.h2_title as title
FROM h2_count
JOIN h2_titles ON h2_count.h2_base_id = h2_titles.h2_id
GROUP BY h2_count.h1_id, h2_count.h2_id


-----------------------------------------------------------------------------------------------
*** Sätze pro Thema
SELECT h1_id, h2_id, h3_id, COUNT(*) as sentences
FROM bun
WHERE h3_id > 0
GROUP BY h1_id, h2_id, h3_id

**** H3 Counter befüllen:
INSERT INTO h3_count (h1_id, h2_id, h3_id, sentences) 
SELECT h1_id, h2_id, h3_id, COUNT(*) as sentences
FROM bun
WHERE h3_id > 0
GROUP BY h1_id, h2_id, h3_id

*** Mit Titel
SELECT h3_count.h3_base_id as id, h3_count.h1_id as Kapitel, h3_count.h2_id as Absatz, h3_count.h3_id as Thema, h3_count.sentences, h3_titles.h3_title as title
FROM h3_count
JOIN h3_titles ON h3_count.h3_base_id = h3_titles.h3_id
GROUP BY Kapitel, Absatz, Thema






Sätze:
bun
bun_bak
bun_hb
bun_length
bun_length_nouse
bun_r


d_force

Absätze
dan
dan_hb
dan_length

df_bun
df_dan
df_h1
df_h2
df_h3

dmark
dstop
fc_bun

genkei

h1
h1_hb
h1_length

h2
h2_hb
h2_length

h3
h3_hb
h3_length

hg_tmp
hgh2
hghi

hinshi
hinshi_setting

hselection

hyoso
hyoso2
hyosobun
hyosobun2

katuyo
katuyo_old

VINAR ID's
khhinshi

outvar
outvar0
outvar1
outvar_lab

rowdata

status
status_char
