touch "root/bund/2021/Präambel/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Präambel/Abschnitt/Was das Land herausfordert.txt"  && timeout /T 3
touch "root/bund/2021/Präambel/Abschnitt/Was wir voranbringen wollen.txt"  && timeout /T 3
touch "root/bund/2021/Präambel/Abschnitt/Wie wir arbeiten wollen.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Verwaltungsmodernisierung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Lebendige Demokratie.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Transparenz.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Föderalismus.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Wahlrecht.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Moderner Staat und Demokratie/Planungs- und Genehmigungsbeschleunigung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitaler Staat und digitale Verwaltung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitale Infrastruktur.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitale Bürgerrechte und IT-Sicherheit.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Nutzung von Daten und Datenrecht.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitale Gesellschaft.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitale Schlüsseltechnologien.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Nachhaltigkeit in der Digitalisierung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Digitale Innovationen und digitale Infrastruktur/Digitale Wirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Zukunftsstrategie Forschung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Innovationen und Transfer.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Forschungsdaten.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Rahmenbedingungen für Hochschule, Wissenschaft und Forschung.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Arbeitsbedingungen in der Wissenschaft.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Internationale Hochschulkooperation.txt"  && timeout /T 3
touch "root/bund/2021/Moderner Staat, digitaler Aufbruch und Innovationen/Innovation, Wissenschaft, Hochschule und Forschung/Wissenschaftskommunikation und Partizipation.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Industrie.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Transformation der Automobilindustrie.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Luft- und Raumfahrt.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Maritime Wirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Einzelhandel.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Gesundheitswirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Tourismus.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Start-up-, Gründungs- und Innovationsförderung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Fairer Wettbewerb.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Bürokratieabbau.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Fachkräfte.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Vergaberecht.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Rohstoffe, Lieferketten und Freihandel.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Wirtschaft/Regionale Wirtschaftsförderung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Naturschutz und Biodiversität.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Natürlicher Klimaschutz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Meeresschutz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Wasserschutz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Luftreinhaltung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Bodenschutz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Chemikalienpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Umwelt- und Naturschutz/Kreislaufwirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Tierschutz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Europäische Agrarpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Ernährung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Landbau.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Digitalisierung in der Landwirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Bodenpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Fischerei.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Landwirtschaft und Ernährung/Lebensmittelmarkt.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Infrastruktur.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Bahnverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Öffentlicher Verkehr und neue Mobilitätsangebote.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Güterverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Autoverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Verkehrsordnung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Radverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Schiffsverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Mobilität/Luftverkehr.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Klimaschutzgesetz.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Erneuerbare Energien.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Kohleausstieg.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Gas und Wasserstoff.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Netze.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Strommarktdesign.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Sozial gerechte Energiepreise.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Klima- und Energieaußenpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Transformation der Wirtschaft.txt"  && timeout /T 3
touch "root/bund/2021/Klimaschutz in einer sozial-ökologischen Marktwirtschaft/Klima, Energie, Transformation/Atom.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Ausbildung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Weiterbildung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Arbeitszeit und Arbeitsort.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Selbständige.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Mindestlohn.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Mini- und Midijobs.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Haushaltsnahe Dienstleistungen.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Befristungen.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Arbeitnehmerüberlassung und Arbeitskräftemobilität.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Tarifautonomie.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Mitbestimmung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Digitale Plattformen.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Arbeit/Arbeits- und Gesundheitsschutz.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Altersvorsorge.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Prävention und Rehabilitation.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Renteneintritt.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Absicherung für Selbständige.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Bürgergeld.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Sozialstaat, Altersvorsorge, Grundsicherung/Inklusion.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Pflege.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Aus- und Weiterbildung in Gesundheit und Pflege.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Öffentlicher Gesundheitsdienst.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Digitalisierung im Gesundheitswesen.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Gesundheitsförderung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Ambulante und stationäre Gesundheitsversorgung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Krankenhausplanung und -finanzierung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Rechte von Patientinnen und Patienten.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Versorgung mit Arzneimitteln und Impfstoffen.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Drogenpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Pflege und Gesundheit/Gesundheitsfinanzierung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Digitalisierung und Vereinfachung.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Klimaschutz im Gebäudebereich.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Schutz der Mieterinnen und Mieter.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Wohneigentum.txt"  && timeout /T 3
touch "root/bund/2021/Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt/Bauen und Wohnen/Städtebau.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Frühkindliche Bildung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Ganztag.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Startchancen-Programm.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Digitalpakt Schule.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Fortbildung für Lehrerinnen und Lehrer.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Ausbildungsförderung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Bildung und Chancen für alle/Erwachsenenbildung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Kinder und Jugend.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Kinderschutz.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Fachkräfte.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Kindergrundsicherung.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Zeit für Familie.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Familienrecht.txt"  && timeout /T 3
touch "root/bund/2021/Chancen für Kinder, starke Familien und beste Bildung ein Leben lang/Kinder, Jugend, Familien und Senioren/Senioren.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Bundespolizeien.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Bevölkerungsschutz.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Zusammenarbeit von Polizei und Justiz.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Justiz.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Kampf gegen Organisierte Kriminalität.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Kampf gegen Extremismus.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Kampf gegen Kindesmissbrauch.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Waffenrecht, Sicherheitsdienste.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Freiheit und Sicherheit.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Verantwortung für Holocaust-Überlebende.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/SED-Opfer.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Kirchen und Religionsgemeinschaften.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Unternehmensrecht.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Schutz der Verbraucherinnen und Verbraucher.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Entscheidung Sterbehilfe.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport/Sport.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Gleichstellung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Gleichstellung/Schutz vor Gewalt.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Gleichstellung/Ökonomische Gleichstellung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Gleichstellung/Reproduktive Selbstbestimmung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Zivilgesellschaft und Demokratie.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Migration, Teilhabe und Staatsangehörigkeitsrecht.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Muslimisches Leben.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Jüdisches Leben.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Queeres Leben.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Rassismus bekämpfen.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Vielfalt/Antidiskriminierung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Soziale Lage in Kunst und Kultur.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Kulturförderung.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Rechtliche Rahmenbedingungen.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Kulturelles Erbe.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Medien.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Erinnerungskultur.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Koloniales Erbe.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Kultur- und Medienpolitik/Auswärtige Kultur- und Bildungspolitik.txt"  && timeout /T 3
touch "root/bund/2021/Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie/Gute Lebensverhältnisse in Stadt und Land/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Zukunft der Europäischen Union.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Rechtsstaatlichkeit.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Wirtschafts- und Währungsunion, Fiskalpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Investitionen.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Soziales Europa.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Krisenfestes Europa.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Europapolitische Koordinierung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Europäische Außen- und Sicherheitspolitik.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Europäische Partner.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Europa/Europäische Freizügigkeit.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Integration, Migration, Flucht/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Integration, Migration, Flucht/Aufenthalts- und Bleiberecht.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Integration, Migration, Flucht/Integration.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Integration, Migration, Flucht/Asylverfahren.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Integration, Migration, Flucht/Europäische und internationale Flüchtlingspolitik.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Multilateralismus.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Abrüstung, Rüstungskontrolle, Rüstungsexporte.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Menschenrechte.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Humanitäre Hilfe.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Zivile Krisenprävention und Friedensförderung.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Verteidigung und Bundeswehr.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Entwicklungszusammenarbeit.txt"  && timeout /T 3
touch "root/bund/2021/Deutschlands Verantwortung für Europa und die Welt/Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte/Bilaterale und regionale Beziehungen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Zukunftsinvestitionen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Haushaltspolitik.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Subventionen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Bund-Länder-Kommunalfinanzen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Steuern.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Vollzug, Vereinfachung und Digitalisierung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Bekämpfung Steuerhinterziehung und Steuergestaltung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Europäische Wirtschafts- und Finanzpolitik.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Bankenunion und Finanzmarktregulierung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Wagniskapitalfinanzierung, Finanzmarkt Deutschland.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Finanzieller Verbraucherschutz und Altersvorsorge.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Sustainable Finance.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Geldwäsche.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Digitale Finanzdienstleistungen und Währungen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Versicherungen.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Wirtschaftsprüfung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Einleitung/Einleitung.txt"  && timeout /T 3
touch "root/bund/2021/Zukunftsinvestitionen und nachhaltige Finanzen/Abschnitt/Kooperation der Koalitionspartner.txt"  && timeout /T 3
touch "root/bund/2021/Arbeitsweise der Regierung und Fraktionen/Abschnitt/Kooperation der Fraktionen.txt"  && timeout /T 3
touch "root/bund/2021/Arbeitsweise der Regierung und Fraktionen/Abschnitt/Arbeit in der Bundesregierung.txt"  && timeout /T 3
touch "root/bund/2021/Arbeitsweise der Regierung und Fraktionen/Abschnitt/Pandemiebekämpfung.txt"  && timeout /T 3
touch "root/bund/2021/Arbeitsweise der Regierung und Fraktionen/Abschnitt/Europapolitische Koordinierung.txt"  && timeout /T 3
touch "root/bund/2021/Arbeitsweise der Regierung und Fraktionen/Abschnitt/Ressortverteilung.txt"  && timeout /T 3