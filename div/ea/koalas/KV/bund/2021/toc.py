t='''
1. Präambel
    1.0.1 Was das Land herausfordert
    1.0.2 Was wir voranbringen wollen
    1.0.3 Wie wir arbeiten wollen
2. Moderner Staat, digitaler Aufbruch und Innovationen
  2.1 Moderner Staat und Demokratie
    2.1.1 Verwaltungsmodernisierung
    2.1.2 Lebendige Demokratie
    2.1.3 Transparenz
    2.1.4 Föderalismus
    2.1.5 Wahlrecht
    2.1.6 Planungs- und Genehmigungsbeschleunigung
  2.2 Digitale Innovationen und digitale Infrastruktur
    2.2.1 Digitaler Staat und digitale Verwaltung
    2.2.2 Digitale Infrastruktur
    2.2.3 Digitale Bürgerrechte und IT-Sicherheit
    2.2.4 Nutzung von Daten und Datenrecht
    2.2.5 Digitale Gesellschaft
    2.2.6 Digitale Schlüsseltechnologien
    2.2.7 Nachhaltigkeit in der Digitalisierung
    2.2.8 Digitale Wirtschaft
  2.3 Innovation, Wissenschaft, Hochschule und Forschung
    2.3.1 Zukunftsstrategie Forschung
    2.3.2 Innovationen und Transfer
    2.3.3 Forschungsdaten
    2.3.4 Rahmenbedingungen für Hochschule, Wissenschaft und Forschung
    2.3.5 Arbeitsbedingungen in der Wissenschaft
    2.3.6 Internationale Hochschulkooperation
    2.3.7 Wissenschaftskommunikation und Partizipation
3. Klimaschutz in einer sozial-ökologischen Marktwirtschaft
  3.1 Wirtschaft
    3.1.1 Industrie
    3.1.2 Transformation der Automobilindustrie
    3.1.3 Luft- und Raumfahrt
    3.1.4 Maritime Wirtschaft
    3.1.5 Einzelhandel
    3.1.6 Gesundheitswirtschaft
    3.1.7 Tourismus
    3.1.8 Start-up-, Gründungs- und Innovationsförderung
    3.1.9 Fairer Wettbewerb
    3.1.10 Bürokratieabbau
    3.1.11 Fachkräfte
    3.1.12 Vergaberecht
    3.1.13 Rohstoffe, Lieferketten und Freihandel
    3.1.14 Regionale Wirtschaftsförderung
  3.2 Umwelt- und Naturschutz
    3.2.1 Naturschutz und Biodiversität
    3.2.2 Natürlicher Klimaschutz
    3.2.3 Meeresschutz
    3.2.4 Wasserschutz
    3.2.5 Luftreinhaltung
    3.2.6 Bodenschutz
    3.2.7 Chemikalienpolitik
    3.2.8 Kreislaufwirtschaft
  3.3 Landwirtschaft und Ernährung
    3.3.1 Tierschutz
    3.3.2 Europäische Agrarpolitik
    3.3.3 Ernährung
    3.3.4 Landbau
    3.3.5 Digitalisierung in der Landwirtschaft
    3.3.6 Bodenpolitik
    3.3.7 Fischerei
    3.3.8 Lebensmittelmarkt
  3.4 Mobilität
    3.4.1 Infrastruktur
    3.4.2 Bahnverkehr
    3.4.3 Öffentlicher Verkehr und neue Mobilitätsangebote
    3.4.4 Güterverkehr
    3.4.5 Autoverkehr
    3.4.6 Verkehrsordnung
    3.4.7 Radverkehr
    3.4.8 Schiffsverkehr
    3.4.9 Luftverkehr
  3.5 Klima, Energie, Transformation
    3.5.1 Klimaschutzgesetz
    3.5.2 Erneuerbare Energien
    3.5.3 Kohleausstieg
    3.5.4 Gas und Wasserstoff
    3.5.5 Netze
    3.5.6 Strommarktdesign
    3.5.7 Sozial gerechte Energiepreise
    3.5.8 Klima- und Energieaußenpolitik
    3.5.9 Transformation der Wirtschaft
    3.5.10 Atom
4. Respekt, Chancen und soziale Sicherheit in der modernen Arbeitswelt
  4.1 Arbeit
    4.1.1 Ausbildung
    4.1.2 Weiterbildung
    4.1.3 Arbeitszeit und Arbeitsort
    4.1.4 Selbständige
    4.1.5 Mindestlohn
    4.1.6 Mini- und Midijobs
    4.1.7 Haushaltsnahe Dienstleistungen
    4.1.8 Befristungen
    4.1.9 Arbeitnehmerüberlassung und Arbeitskräftemobilität
    4.1.10 Tarifautonomie
    4.1.11 Mitbestimmung
    4.1.12 Digitale Plattformen
    4.1.13 Arbeits- und Gesundheitsschutz
  4.2 Sozialstaat, Altersvorsorge, Grundsicherung
    4.2.1 Altersvorsorge
    4.2.2 Prävention und Rehabilitation
    4.2.3 Renteneintritt
    4.2.4 Absicherung für Selbständige
    4.2.5 Bürgergeld
    4.2.6 Inklusion
  4.3 Pflege und Gesundheit
    4.3.1 Pflege
    4.3.2 Aus- und Weiterbildung in Gesundheit und Pflege
    4.3.3 Öffentlicher Gesundheitsdienst
    4.3.4 Digitalisierung im Gesundheitswesen
    4.3.5 Gesundheitsförderung
    4.3.6 Ambulante und stationäre Gesundheitsversorgung
    4.3.7 Krankenhausplanung und -finanzierung
    4.3.8 Rechte von Patientinnen und Patienten
    4.3.9 Versorgung mit Arzneimitteln und Impfstoffen
    4.3.10 Drogenpolitik
    4.3.11 Gesundheitsfinanzierung
  4.4 Bauen und Wohnen
    4.4.1 Digitalisierung und Vereinfachung
    4.4.2 Klimaschutz im Gebäudebereich
    4.4.3 Schutz der Mieterinnen und Mieter
    4.4.4 Wohneigentum
    4.4.5 Städtebau
5. Chancen für Kinder, starke Familien und beste Bildung ein Leben lang
  5.1 Bildung und Chancen für alle
    5.1.1 Frühkindliche Bildung
    5.1.2 Ganztag
    5.1.3 Startchancen-Programm
    5.1.4 Digitalpakt Schule
    5.1.5 Fortbildung für Lehrerinnen und Lehrer
    5.1.6 Ausbildungsförderung
    5.1.7 Erwachsenenbildung
  5.2 Kinder, Jugend, Familien und Senioren
    5.2.1 Kinder und Jugend
    5.2.2 Kinderschutz
    5.2.3 Fachkräfte
    5.2.4 Kindergrundsicherung
    5.2.5 Zeit für Familie
    5.2.6 Familienrecht
    5.2.7 Senioren
6. Freiheit und Sicherheit, Gleichstellung und Vielfalt in der modernen Demokratie
  6.1 Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport
    6.1.1 Bundespolizeien
    6.1.2 Bevölkerungsschutz
    6.1.3 Zusammenarbeit von Polizei und Justiz
    6.1.4 Justiz
    6.1.5 Kampf gegen Organisierte Kriminalität
    6.1.6 Kampf gegen Extremismus
    6.1.7 Kampf gegen Kindesmissbrauch
    6.1.8 Waffenrecht, Sicherheitsdienste
    6.1.9 Freiheit und Sicherheit
    6.1.10 Verantwortung für Holocaust-Überlebende
    6.1.11 SED-Opfer
    6.1.12 Kirchen und Religionsgemeinschaften
    6.1.13 Unternehmensrecht
    6.1.14 Schutz der Verbraucherinnen und Verbraucher
    6.1.15 Entscheidung Sterbehilfe
    6.1.16 Sport
  6.2 Gleichstellung
    6.2.1 Schutz vor Gewalt
    6.2.2 Ökonomische Gleichstellung
    6.2.3 Reproduktive Selbstbestimmung
  6.3 Vielfalt
    6.3.1 Zivilgesellschaft und Demokratie
    6.3.2 Migration, Teilhabe und Staatsangehörigkeitsrecht
    6.3.3 Muslimisches Leben
    6.3.4 Jüdisches Leben
    6.3.5 Queeres Leben
    6.3.6 Rassismus bekämpfen
    6.3.7 Antidiskriminierung
  6.4 Kultur- und Medienpolitik
    6.4.1 Soziale Lage in Kunst und Kultur
    6.4.2 Kulturförderung
    6.4.3 Rechtliche Rahmenbedingungen
    6.4.4 Kulturelles Erbe
    6.4.5 Medien
    6.4.6 Erinnerungskultur
    6.4.7 Koloniales Erbe
    6.4.8 Auswärtige Kultur- und Bildungspolitik
  6.5 Gute Lebensverhältnisse in Stadt und Land
7. Deutschlands Verantwortung für Europa und die Welt
  7.1 Europa
    7.1.1 Zukunft der Europäischen Union
    7.1.2 Rechtsstaatlichkeit
    7.1.3 Wirtschafts- und Währungsunion, Fiskalpolitik
    7.1.4 Investitionen
    7.1.5 Soziales Europa
    7.1.6 Krisenfestes Europa
    7.1.7 Europapolitische Koordinierung
    7.1.8 Europäische Außen- und Sicherheitspolitik
    7.1.9 Europäische Partner
    7.1.10 Europäische Freizügigkeit
  7.2 Integration, Migration, Flucht
    7.2.1 Aufenthalts- und Bleiberecht
    7.2.2 Integration
    7.2.3 Asylverfahren
    7.2.4 Europäische und internationale Flüchtlingspolitik
  7.3 Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte
    7.3.1 Multilateralismus
    7.3.2 Abrüstung, Rüstungskontrolle, Rüstungsexporte
    7.3.3 Menschenrechte
    7.3.4 Humanitäre Hilfe
    7.3.5 Zivile Krisenprävention und Friedensförderung
    7.3.6 Verteidigung und Bundeswehr
    7.3.7 Entwicklungszusammenarbeit
    7.3.8 Bilaterale und regionale Beziehungen
8. Zukunftsinvestitionen und nachhaltige Finanzen
    8.0.1 Zukunftsinvestitionen
    8.0.2 Haushaltspolitik
    8.0.3 Subventionen
    8.0.4 Bund-Länder-Kommunalfinanzen
    8.0.5 Steuern
    8.0.6 Vollzug, Vereinfachung und Digitalisierung
    8.0.7 Bekämpfung Steuerhinterziehung und Steuergestaltung
    8.0.8 Europäische Wirtschafts- und Finanzpolitik
    8.0.9 Bankenunion und Finanzmarktregulierung
    8.0.10 Wagniskapitalfinanzierung, Finanzmarkt Deutschland
    8.0.11 Finanzieller Verbraucherschutz und Altersvorsorge
    8.0.12 Sustainable Finance
    8.0.13 Geldwäsche
    8.0.14 Digitale Finanzdienstleistungen und Währungen
    8.0.15 Versicherungen
    8.0.16 Wirtschaftsprüfung
9. Arbeitsweise der Regierung und Fraktionen
    9.0.1 Kooperation der Koalitionspartner
    9.0.2 Kooperation der Fraktionen
    9.0.3 Arbeit in der Bundesregierung
    9.0.4 Pandemiebekämpfung
    9.0.5 Europapolitische Koordinierung
    9.0.6 Ressortverteilung
'''