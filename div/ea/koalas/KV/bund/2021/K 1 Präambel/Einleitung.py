t='''
Das Ergebnis der Bundestagswahl verstehen unsere drei Parteien als Auftrag, eine gemeinsame Regierungskoalition zu bilden.
Wir haben unterschiedliche Traditionen und Perspektiven, doch uns einen die Bereitschaft, gemeinsam Verantwortung für die Zukunft Deutschlands zu übernehmen, das Ziel, die notwendige Modernisierung voranzutreiben, das Bewusstsein, dass dieser Fortschritt auch mit einem Sicherheitsversprechen einhergehen muss und die Zuversicht, dass dies gemeinsam gelingen kann. Wir verpflichten uns, dem Wohle aller Bürgerinnen und Bürger zu dienen.
'''
