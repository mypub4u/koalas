t='''
Deutschland ist vielfältig und die Gesellschaft ist freier und reicher an Perspektiven, aber auch komplexer und widersprüchlicher geworden. Gemeinsinn, Solidarität und Zusammenhalt sind neu zu bestimmen. Das gelingt nicht, indem man über Unterschiede hinweggeht, sondern indem die unterschiedlichen Stimmen in unserem Land gleichberechtigt Gehör finden.
In diesem Sinne spiegelt eine Koalition aus unseren drei so unterschiedlichen Parteien auch einen Teil der komplexen gesellschaftlichen Wirklichkeit wider. Wenn wir es schaffen, gemeinsam die Dinge voranzutreiben, kann das ein ermutigendes Signal in die Gesellschaft hinein sein: dass Zusammenhalt und Fortschritt auch bei unterschiedlichen Sichtweisen gelingen können.
Wir wollen eine Kultur des Respekts befördern – Respekt für andere Meinungen, für Gegenargumente und Streit, für andere Lebenswelten und Einstellungen. Der vorliegende Koalitionsvertrag zeigt, dass wir tragfähige gemeinsame Lösungen gefunden haben. Er bildet die Arbeitsgrundlage für unsere Koalition.
'''


