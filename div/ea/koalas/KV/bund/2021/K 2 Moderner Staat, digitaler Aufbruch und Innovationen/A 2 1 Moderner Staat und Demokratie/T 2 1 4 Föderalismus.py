t='''
Der Föderalismus ist eine Grundsäule der Bundesrepublik. Um die Leistungsfähigkeit zu erhöhen, braucht es Klarheit bei den Aufgaben und der Finanzierung. Wir streben eine engere, zielgenauere und verbindliche Kooperation zwischen Bund, Ländern und Kommunen an. Dazu werden wir gemeinsam mit Kommunen und Ländern einen Föderalismusdialog zur transparenteren und effizienteren Verteilung der Aufgaben, insbesondere zu den Themen Katastrophen- und Bevölkerungsschutz, Bildung und Innere Sicherheit sowie zur Nutzung der Möglichkeiten der Digitalisierung, führen.
Wir stehen zum Bonn-Berlin-Gesetz. Hierzu wird der Bund mit der Region Bonn sowie den Ländern Nordrhein-Westfalen und Rheinland-Pfalz eine vertragliche Zusatzvereinbarung abschließen.
'''
