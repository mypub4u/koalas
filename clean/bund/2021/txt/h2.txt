<h2>1. Moderner Staat und Demokratie</h2>
<h2>2. Digitale Innovationen und digitale Infrastruktur</h2>
<h2>3. Innovation, Wissenschaft, Hochschule und Forschung</h2>

<h2>1. Wirtschaft</h2>
<h2>2. Umwelt- und Naturschutz</h2>
<h2>3. Landwirtschaft und Ernährung</h2>
<h2>4. Mobilität</h2>
<h2>5. Klima, Energie, Transformation</h2>

<h2>1. Arbeit</h2>
<h2>2. Sozialstaat, Altersvorsorge, Grundsicherung</h2>
<h2>3. Pflege und Gesundheit</h2>
<h2>4. Bauen und Wohnen</h2>

<h2>1. Bildung und Chancen für alle</h2>
<h2>2. Kinder, Jugend, Familien und Senioren</h2>

<h2>1. Innere Sicherheit, Bürgerrechte, Justiz, Verbraucherschutz, Sport</h2>
<h2>2. Gleichstellung</h2>
<h2>3. Vielfalt</h2>
<h2>4. Kultur- und Medienpolitik</h2>
<h2>5. Gute Lebensverhältnisse in Stadt und Land</h2>

<h2>1. Europa</h2>
<h2>2. Integration, Migration, Flucht</h2>
<h2>3. Außen, Sicherheit, Verteidigung, Entwicklung, Menschenrechte</h2>
