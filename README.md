# KOALitionsvertags-Analysen (KOALA)

Hier können wir im Laufe der Zeit die Koalitionsverträge bzw. Regierungsprogramme deutscher Regierungen auf Bund und Länderebene sammeln, analysieren und vergleichen.

<p align="center">
  <img width="40%" src="koala_bleeptrack.jpg" />
</p>
painted with <3 by [bleeptrack](https://bleeptrack.de/)


Motivation:
---

- Neugierde
- Spaß an Sourcecode (lesen, schreiben, analysieren)
- Überraschungen entdecken (177 Seiten, 76 Seiten einfach nur Luft)
- Was lernen über Tools, Methoden speziell für quantitative Textanalyse.
- Zusammenhänge erkennen und vernetzen (TreeMaps, erstaunliche Text-Visuals)
- Vergleiche anstellen. Wie schlägt sich die KOALA ( KOALitionsvertrags Analyse) ...
  - ... vs. Coding-Rules (Text als Code)
  - ... vs. OOM / OOD Prinzipien (Text als Objekt-Modell)
  - ... vs. Requirement Quality Gates (Text als Spezifikation)
  - ... vs. Literarischen Ansprüchen (Text als Literatur)
  - ... vs. Anderen Koalas. (16 Bundesländer, 1-2 Wahlperioden)

Vier Sichtweisen:
---

1. Text als Sourcecode ... 
2. Text als Objekt-Modell ...
3. Text als Spezifikation ...
4. Text als Literatur ... 



##### zu 1: Text als Code:

...für sca-a-like quantitative analysen

- Das K.A.T. Prinzip:
  - Kapitel (h1). Oft mit einer römischen Zahl beginnend.
  - Abschnitt (h2). Oft lateinisch nummeriert
  - Thema (h3): Einzeilige Übeschrift ohne Punkt am Ende der Zeile
- Ein Absatz wird mit einem Zeilenumbruch, i.d.R. \n begrenzt
- Ein Satz wird mit einem ". " begrenzt. Problem Abkürzungen u.a., usw. z.b. global ersetzen
- Ein Wort wird durch " " oder ". " abgesschlossen.
 

##### zu 2: Text als Objekt-Modell:

 ... für Struktur-Anlalyse im Sinne von OOM/OOD
 - verben: Methoden
 - Hauptwörter: Klassen
 - Alles andere: Properties & Parameter
 - Kapitel-Struktur als Folder & Packages
 - evt. Klassenmodell halbautomatisch (reverse) engineeren, evt. mit EA, VP.
 Vorgehen:
 - Dekomposition in Root-Folder Kapitel-Kurz-ID mit führenden Nummern, z.b "01-Präambel", "02-Digitalisierung", "03-Wirtschaft_Umwelt"
 - Anlegen der 22 Abschnitte als Sub-Folder, "Haushaltspolitik". Das Erstell-Datum wird zum Sortierkriterium der Reihenfolge.
 - Anlengen der 200 Themen als Textdatei. "


##### zu 3: Text als Spezifikation ...

 ... für inhaltliche Analysen
 - Capability, Epic, Feature/Enabler, Story, Tasks
 - plus Kadenz, plus Objectives -> Abgl. mit SAFe 5.x

###### Verständniss & Vorgehen: 
Der Ampel-Vertrag ist wie ein Versprechen etwas in der Zukunft tuen zu wollen. Ähnliches passiert während eines PI-Planing im SAFe-Umfeld. Dort wird am Ende über sog. Objectives kommuniziert wozu sich ein Team "comitted" hat. Das Team-Ampel schreibt im Koalitionsvertrag auch nichts anderes. Sie "wollen", und Sie "werden" insgesamt 916x im Ampel-Vertrag. 

wollen-werden-müssen-machen-prüfen

###### 2021:

Aufgeteilt wie folgt:
- (wir) wollen = 280 (1x bis)
- wollen wir = 188 (2x bis)
- wollen (494x) ohne wir (26)

- wir werden = 341 (1x bis)
- werden wir = 307 (3x bis)
- werden (987) ohne wir (339)

- müssen (60x) nur 2x im Zusammenhang mit "wir"

- machen (71), 16x mit "wir" 

- prüfen (79x), 27x mit "wir"  Sollbruch? Spyke?


###### 2018:
- (wir) wollen = 468 (0x bis)
- wollen (wir) = 402 (1x bis)
- wollen (906) ohne wir (36)

- wir werden = 310 (2x bis)
- werden wir = 308 (1x bis)
- werden (1036) ohne wir (418)

- müssen (74x), 6x mit "wir"

- machen (46), 3x mit "wir"

- prüfen (109x), 27 mit "wir" (Generelle Prüfung in der Mitten Legi)








Und dann wäre da noch "prüfen" 79x (Die Sollbruchstellen? Braucht evt. einen Spyke)

Die Überlegung ist nun pro Thema alle "Wollen & Werden" als Objectives aufzulisten. 


##### zu 4 Text als Literatur ...

 ... für eine Literatur-Analyse
 - Grammatik, Schreibstil, 
 - Stimmung, Deutschlehrer


Methoden & Vorgehen:
---
- Vermessen, Taxieren, Vergleichen
- LIX (Lesbarkeitsindex) mit anderen Koalas vergleichen
- Unterschiedliche Aspekte untersuchen, wie
  - Rechtschreib- und Grammatikprüfung
  - Passivsätze
  - Phrasen
  - Abkürzungen
  - Zahlen
  - Unpersönliche Sprache
  - Füllwörter
  - Nominalstil
  - Kommata/Satzbau
  - Lange Wörter / Sätze
- Vermessung nach ausgedruckten Seiten nicht zielführend -> Schrift, Zeilenabstand, ... zu unterschiedlich.

Erste Messungen (Koala Bund 2021):
___
- 3392 Sätze
- 947 Absätze
- 230 Fremdwörter, kein Glossar
- 9 Kapitel (1.)
- 22 Abschnitte (1.1)
- 200 Themen (1.1.1)
- Lesedauer ~45 Minuten

Erste Findings:
___

- Ausgelieferte Version mit 177 DIN-A4 Seiten ist etwas unleserlich und aufgebläht. 
Durch Änderung der Zeilenabstände sowie Absatz-, Seitenränder auf "Normal" schrumpft der Vertrag von 177 auf 101 Seiten; und wird auch gleich viel besser lesbar. 

- Sidefact: Würde der Vertrag in dieser Form 1000 mal ausgedruckt, wären 76.000 Seiten überflüssig. Wir kännten hier 57% Co2 und Papier/Holz sparen.

- Interessante Verhältnisse:
 - Radwege kommt 1x vor, Autobahn 3x.
 - Konkreter: 
   - Autoverkehr: 19 Sätze, 360 Wörter, FLX: -7
   - Radverkehr: 3 Sätze, 54 Wörter, FLX: 1

Was fehlt?
---

Wörter die nicht(!) vorkommen (Wordcloud mit Google-Findings taxieren):
- Gemeinwohl-Ökonomie 1,15 Mio
- GWÖ 0,06 Mio
- Gemeinwohl-Bilanz 0,3 Mio
- Verkehrswende 1,08 Mio
- Vermögenssteuer 1,24 Mio
- Klimanotstand 0,269 Mio
- Maschinensteuer 0,067
- Empathie 10,7 Mio
- Säkularisierung 0,494 Mio 
- Tempolimit 1,140
- Kohlesubvention 0,013
- Dieselsubvention 0,004 Mio
- Umverteilung 1,550 Mio
- Schiffsemison 0,0277 Mio  
- empirisch 3,77
- Föderalismusreform 0,172


Such mal nach dem hier:

- Politikökonomie
- Transformationsforschung


----------------------------------------------------------
Vergleich 2018 vs. 2021

Cyber (2018 insgesamt 18x, 2021 nur 13x)
idealerweise 3x (2x im Zusammhang mit dem Kohlausstieg 2030)

wir werden = 341 (1x bis)
werden wir = 307 (3x bis)

(wir) wollen = 280 (1x bis)
wollen wir = 188 (2x bis)
"bis 20" = 21x
"ab 20" = 12x


2018 = 1
2020 = 5
2020er = 3

2021 = 6
2022 = 37
2023 = 22
2024 = 4
2025 = 11

2026 = 3
2027 = 2
2029 = 1
2030 = 27
2035 = 2
2040 = 1
2045 = 5
2060 = 1


- zähle sollen und müssen
- DIN / Industrie / Norem
- Games / eSport
- Idealerweise / Optimalerweise / Wenn möglich

Auch mal ein paar echt tolle Sachen:

- Daher streichen wir § 219a StGB.
- Abgabe von Cannabis an Erwachsene zu Genusszwecken
- werden Wahlalter zum "EU-Parla" auf 16
- wollen das Grundgesetzt ändern ... Wahlalter auf 16 ... für Bund
- werden wird ... EEG-Umlage ... beenden. (Allerdings ohne Jahreszahl)
- DB Netz als 100% wird "gemeinwohlorientiert"
- Atomausstieg bleibt
- Kein Atom Greenwashing: Atomenergie ... kommt selber für ihr Kosten auf.
- Bleiberecht nach 4 bis 6 Jahren
- Kettenduldung wird Chanden-Aufenthaltsrecht 
Allerdings:  
Ein "Einwanderungs- und Aufenthaltsgesetzbuch" wird es nur idealerweise geben.
- [Netzpolitik spotten]

Weitere Themen-Brainstorm (tbd):
-----

- Einladung: Macht mit, lass uns ein Museum der Koalas bauen. Vielleicht finden wir Methoden, wie wir Akzeptanzkriterien und S.M.A.R.T'ness einbauen oder provozieren können.

- Versprechen-Zeitstrahl aufbereiten. Was wird bis wann versprochen, und was davon liegt überhaupt in der Legislatur-Periode.

- Vertrag wie PI-Objectives (SAFe 5.x) verstehen und prüfen ob diese S.M.A.R.T. sind. 

- Vorsicht (nochmal genauer überprüfen):
  - Im Vergleich zu Ausführungen zu Europa, Sicherheit oder Bundeswehr, wirken die beiden Sätze zur Pandemiebekämpfung, hurtig dran geschustert. 
  - Beispiel: Zum Thema FinTech & Kryptokrams, gibt es 206 Wörter zu lesen zur Pandemiebekämpfung 35. Ok Pandemiebekämpfung ist halt nicht so sexy wie die neuesten Robo-Advisors. 
  - Würde man Prämbel und Ministerverteil-Kapitel weg lassen, also nur die reinen "Content"-Kapitel betrachten, Pandamie würde nicht ein einziges mal auftauchen.

- Und nein, Klimaschutz steht nicht an erster Stelle. Klimaschutz findet in Kapitel 3 statt. Aber immerhin im Vergleich zu anderen Koalitionsverträgen ist das schon ein Aufstieg. (Ranking des Umwelt-Thema in den 16 Länder Koalas) Und vom Umfang her das größte.

- Vergleich zu anderen Koalas. Bis auf Bremen, Hamburg und BaWü gibt es nur eher "abschreckende" PDF-Exemplare. Lesbarkeit, Aufbereitung, Themenschwerpunkte. (evt. Treemaps)

- Wie können wir die "Objectives" bzw. die Zielerreichung einer Regierung messen? Mit Akzeptanzkriterien.

- Regierungszeit -und Ergebnisse vs. Koalitionsverträge. Erfolgsmessung.


Konklusio:
-----
Erst kommen Wahlversprechen, dann Koalitionsversprechen, dann kommt die Realität. Kommen dann wieder die Ausreden?
Versprochen habt ihr uns schon viel, und oft. Jetzt wollen wir Euch handeln sehen.



Tools:
---
- khcoder
- open office
- ms word & excel
- vs code
- graphviz
- rlang für wordclouds & treemaps & tm
- leichtlesbar.ch (evt)
- psychometrika (evt)
- Apache Mahout, UIMA, OpenNLP (evt)
- Visual Paradigm (evt)
- Enterprise Architect (evt)
